test_that("fizzbuzz works for 1", {
  expect_equal(fizzbuzz(1), "1")
})

test_that("fizzbuzz works for 2", {
  expect_equal(fizzbuzz(2), "2")
})

test_that("fizzbuzz returns fizz upon receiving 3", {
  expect_equal(fizzbuzz(3), "fizz")
})

test_that("fizzbuzz returns buzz upon receiving 5", {
  expect_equal(fizzbuzz(5), "buzz")
})

test_that("fizzbuzz returns fizzbuzz upon receiving 15", {
  expect_equal(fizzbuzz(15), "fizzbuzz")
})

test_that("fizzbuzz returns fizz upon receiving 6", {
  expect_equal(fizzbuzz(6), "fizz")
})

test_that("fizzbuzz returns fizz upon receiving 10", {
  expect_equal(fizzbuzz(10), "buzz")
})

test_that("fizzbuzz returns fizz upon receiving 30", {
  expect_equal(fizzbuzz(30), "fizzbuzz")
})

test_that("fizzbuzz returns character", {
  expect_type(fizzbuzz(1), "character")
})

test_that("fizzbuzz throws error when x is not a number", {
  expect_error(fizzbuzz("1"))
})
